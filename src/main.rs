use riff;
use std::{
    collections::HashSet,
    fmt::Display,
    fs::File,
    io::{BufReader, Read, Seek, SeekFrom, Write},
    ops::Index,
    time::Instant,
};

const CHUNK_HEADER_SIZE: u32 = 8;

/// String matcher. Add bytes one by one, and test whether the requested string has been matched.
struct Matcher {
    pattern: &'static [u8],
    matched_len: usize,
}

impl Matcher {
    /// Construct with pattern to find
    fn new(pattern: &'static [u8]) -> Self {
        Matcher { pattern, matched_len: 0 }
    }

    /// Add a byte, returning true if a match is complete
    fn add(&mut self, byte: u8) -> bool {
        // If there is a match in progress but this byte fails to extend the match, reset
        // the match state
        if self.matched_len > 0 && byte != self.pattern[self.matched_len] {
            self.matched_len = 0;
        }

        // If this byte matches, extend the match (or start a new match if matched_len is
        // currently 0)
        if byte == self.pattern[self.matched_len] {
            self.matched_len += 1;
        }

        if self.matched_len == self.pattern.len() {
            // Match complete
            self.matched_len = 0;
            true
        } else {
            // No match so far
            false
        }
    }

    /// Length of pattern being matched
    fn pattern_len(&self) -> usize {
        self.pattern.len()
    }
}

type Chunks = Vec<riff::Chunk>;

fn is_valid_wav(chunk: &riff::Chunk, mut file: &mut File) -> bool {
    // Reading contents may fail if this is not actually a valid RIFF chunk
    let contents = chunk.read_contents(&mut file);
    if let Ok(contents) = contents {
        if &contents[0..4] == b"WAVE" {
            // Every WAV should have two subchunks: "fmt " and "data"
            let subchunks: HashSet<_> = chunk.iter(&mut file).map(|c| c.id().value).collect();
            if subchunks.contains(b"fmt ") && subchunks.contains(b"data") {
                return true;
            }
        }
    }

    false
}

/// Representation of a single file containing concatenated sound file chunks
#[derive(Debug)]
struct ChunkedFile {
    // Source file
    source_file: File,

    // Vector of chunks
    chunks: Chunks,

    // Total size in bytes
    file_size: usize,

    // Total number of chunks parsed (including invalid)
    total_parsed_chunks: usize,
}

impl ChunkedFile {
    /// Construct a ChunkedFile by opening a file on disk
    fn from_file(mut file: File) -> ChunkedFile {
        // Create reader to read the file
        let reader = BufReader::new(&file);

        // Match incoming bytes and record start positions of RIFF headers
        let mut chunk_starts = Vec::new();
        let mut riff_matcher = Matcher::new(b"RIFF");
        let mut file_size = 0;
        for (i, maybe_byte) in reader.bytes().enumerate() {
            let byte = maybe_byte.unwrap();
            if riff_matcher.add(byte) {
                // Match found
                chunk_starts.push(i - (riff_matcher.pattern_len() - 1));
            }

            // Keep track of the size
            file_size = i
        }

        // Parse each header using the riff::Chunk parser and build a vector of parsed chunks
        file.rewind().expect("failed to rewind file");
        let mut chunks = Vec::new();
        let mut total_parsed_chunks = 0;
        for startpos in chunk_starts {
            // Keep track of total chunk count for status information
            total_parsed_chunks += 1;

            // Only add the chunk if it parses as a valid WAV
            let chunk = riff::Chunk::read(&mut file, startpos as u64).unwrap();
            if is_valid_wav(&chunk, &mut file) {
                chunks.push(chunk);
            }
        }

        ChunkedFile { source_file: file, chunks, file_size, total_parsed_chunks }
    }

    /// Return number of audio chunks in the file
    fn len(&self) -> usize {
        self.chunks.len()
    }

    /// Load audio data from the given chunk index
    fn get_data(&mut self, chunk_index: usize) -> std::io::Result<Vec<u8>> {
        let chunk = &self.chunks[chunk_index];
        let mut stream = BufReader::new(&self.source_file);

        // riff::Chunk::read_contents() gives us the chunk contents minus the chunk header,
        // so dumping this would not give a full WAV file. Instead we just read the bytes
        // ourselves.
        stream.seek(SeekFrom::Start(chunk.offset()))?;
        let total_chunk_size = chunk.len() + CHUNK_HEADER_SIZE;
        let mut taker = stream.take(u64::from(total_chunk_size));

        let mut result = Vec::new();
        taker.read_to_end(&mut result)?;

        Ok(result)
    }
}

impl Index<usize> for ChunkedFile {
    type Output = riff::Chunk;

    fn index(&self, index: usize) -> &Self::Output {
        &self.chunks[index]
    }
}

impl Display for ChunkedFile {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let unrecognised = self.total_parsed_chunks - self.chunks.len();
        writeln!(
            f,
            "{} RIFF chunks [{} WAVEs, {} unrecognised]",
            self.total_parsed_chunks,
            self.chunks.len(),
            unrecognised
        )?;
        Ok(())
    }
}

fn main() {
    let args: Vec<_> = std::env::args().collect();
    if args.len() != 2 {
        println!("Missing WAV file argument");
        return;
    }

    let instant = Instant::now();

    let stream = File::open(&args[1]).expect("failed to open file");
    let mut audio_file = ChunkedFile::from_file(stream);
    println!("File: {}", args[1]);
    println!("Audio chunks: {}", audio_file.len());

    let elapsed_secs = instant.elapsed().as_millis() as f32 / 1000.0;
    let total_mb = audio_file.file_size as f32 / 1048576.0;
    let mb_per_sec = total_mb / elapsed_secs;
    println!("Size: {} MB", total_mb);
    println!("Elapsed time: {} seconds [{} MB/s]", elapsed_secs, mb_per_sec);

    println!("Contents: {}", audio_file);

    for chunk_index in 0..audio_file.len() {
        let content = audio_file.get_data(chunk_index).expect("failed to get content");
        let filename = format!("{}.wav", chunk_index);
        let mut outfile = File::create(filename).expect("failed to create output file");
        outfile.write_all(&content).expect("failed to write output file");
    }
}

#[cfg(test)]
mod tests {
    use core::num;
    use once_cell::sync::Lazy;
    use std::{fs::File, path::PathBuf};

    use crate::*;

    // Filesystem path to main testdata directory
    const MANIFEST_DIR: Lazy<PathBuf> = Lazy::new(|| PathBuf::from(env!("CARGO_MANIFEST_DIR")));
    const BASE_DIR: Lazy<PathBuf> = Lazy::new(|| MANIFEST_DIR.join("testdata"));

    fn load_test_file(name: &str) -> File {
        File::open(BASE_DIR.join(name)).unwrap()
    }

    #[test]
    fn matcher_can_match() {
        let mut matcher = Matcher::new(b"RIFF");

        assert!(!matcher.add(b'G'));
        assert!(!matcher.add(b'R'));
        assert!(!matcher.add(b'I'));
        assert!(!matcher.add(b'F'));
        assert!(matcher.add(b'F')); // matches

        assert!(!matcher.add(b'F'));
        assert!(!matcher.add(b'R'));
        assert!(!matcher.add(b'I'));
        assert!(!matcher.add(b'R'));
        assert!(!matcher.add(b'I'));
        assert!(!matcher.add(b'F'));
        assert!(matcher.add(b'F')); // matches
    }

    #[test]
    fn load_simple_wav() {
        let file = load_test_file("sinewave.wav");
        let chunks = ChunkedFile::from_file(file);
        assert_eq!(chunks[0].offset(), 0);
    }

    #[test]
    fn load_concat_wav() {
        let file = File::open(BASE_DIR.join("sine3.wav")).unwrap();
        let chunks = ChunkedFile::from_file(file);
        assert_eq!(chunks.len(), 3);
        assert_eq!(chunks[0].offset(), 0);
        assert_eq!(chunks[1].offset(), 88244);
        assert_eq!(chunks[2].offset(), 176488);
    }

    #[test]
    fn parse_wav_from_chunk() {
        // Open test file
        let file = File::open(BASE_DIR.join("sine3.wav")).unwrap();

        // Reference file (this is repeated 3 times in the test file)
        let mut ref_file = File::open(BASE_DIR.join("sinewave.wav")).unwrap();
        let mut ref_contents = Vec::new();
        ref_file.read_to_end(&mut ref_contents).unwrap();

        let mut chunks = ChunkedFile::from_file(file);
        assert_eq!(chunks.chunks[0].id().as_str(), "RIFF");
        assert_eq!(chunks.chunks[1].id().as_str(), "RIFF");
        assert_eq!(chunks.chunks[2].id().as_str(), "RIFF");

        let first_wav = chunks.get_data(0).unwrap();
        assert_eq!(first_wav.len(), ref_contents.len());
        let second_wav = chunks.get_data(1).unwrap();
        assert_eq!(second_wav.len(), ref_contents.len());
        let third_wav = chunks.get_data(2).unwrap();
        assert_eq!(third_wav.len(), ref_contents.len());

        // Compare the concatenated WAV contents with the single sinewave reference file
        assert_eq!(ref_contents[0..32], first_wav[0..32]);
        assert_eq!(ref_contents[0..32], second_wav[0..32]);
        assert_eq!(ref_contents[0..32], third_wav[0..32]);
    }

    #[test]
    fn skip_non_wave_riff() {
        let file = load_test_file("wave_with_junk.dat");
        let mut chunked_file = ChunkedFile::from_file(file);

        // Only the WAV chunks should be loaded
        assert_eq!(chunked_file.len(), 2);
        assert_eq!(chunked_file.get_data(0).unwrap().len(), 88244);
        assert_eq!(chunked_file.get_data(1).unwrap().len(), 88244);
    }

    #[test]
    fn read_ogg_file() {
        let mut file = load_test_file("sinewave.ogg");
        let mut reader = ogg::PacketReader::new(&mut file);

        let mut data = Vec::new();
        while let Some(mut packet) = reader.read_packet().expect("failed to read packet") {
            assert_eq!(
                packet.stream_serial(),
                0x5548c3dc /* file stream serial from ogginfo */
            );
            data.append(&mut packet.data);
        }
        assert_eq!(data.len(), 4935);
    }
}
